#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // RX, TX
char buffer[100] = {0};
long prev = 0;
long prevV = 0;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
boolean request = false;

//Assigning to Analog Pins.
unsigned int pot_pin = 3; 

//Assigning to Digital Pins.
unsigned int pwm = 5;
unsigned int dir = 3;


void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  Serial.println("Init!");
  pinMode(dir, OUTPUT);

  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);

  prev = millis();
  prevV = millis();
}


void loop() // run over and over
{
  handleCurrent();
  handleVoltage();
  softSerialEvent();
  handleSerial();
  serialEvent();
  
  int value = analogRead(pot_pin);
  Serial.print("P");
  Serial.print(value);
  Serial.println("#");  
  delay(500);   
}

void softSerialEvent() {
  while (mySerial.available()) {
    if(!request) {
      char inChar = (char)mySerial.read();
      if(inChar == '&') {
        request = true;
      }
    } else {
      // get the new byte:
      char inChar = (char)mySerial.read();
      // add it to the inputString:
      if (inChar == '\n') {
        stringComplete = true;
         request = false;
        return;
      } else if(inChar == '&') {
        request = true;
        inputString = "";
      }
      inputString += inChar;
      // if the incoming character is a newline, set a flag
      // so the main loop can do something about it:
    }
  }
}

float map_float(int val, int min1, int max1, float min2, float max2){
  return (((float)val-min1)*(max2-min2)/(max1-min1) + min2);
}

char fbuf[30] = {0};

char *ftoa(char *a, double f, int precision)
{
 long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};
 
 char *ret = a;
 long heiltal = (long)f;
 itoa(heiltal, a, 10);
 while (*a != '\0') a++;
 *a++ = '.';
 long decimal = abs((long)((f - heiltal) * p[precision]));
 itoa(decimal, a, 10);
 
 //Serial.print("f and decimal: ");
 //Serial.print(f);
 //Serial.print(" , ");
 //Serial.println(decimal);
 
 return ret;
}

void handleCurrent() {
 if((millis()-prev) >= 1000) {
    float cur = map_float(analogRead(A2), 0, 1023, -15.0f, 15.0f);
    ftoa(fbuf, cur, 5);
    sprintf(buffer, "!C%s#", fbuf);
    //mySerial.println(buffer);
    Serial.print("C");
    Serial.print(cur);
    Serial.println("#");
    //Serial.println(buffer);
    prev = millis();
    
  } 
}
void handleVoltage() {
  if((millis()-prevV) >= 1000) {
    float vol = map_float(analogRead(A1), 0, 1023, 24.0f, 29.0f);
    ftoa(fbuf, vol, 5);
   
    sprintf(buffer, "!V%s#",fbuf);
    //mySerial.println(buffer);
    
    float vol_accurate = (1.1439*vol)-3.4021;
  
  /*float vol_accurate
    = 0.252*vol*vol
    - 12.234*vol
    + 174.07;
 */
    
    //Serial.println("fbuf ");
    //Serial.println(fbuf);
    //Serial.println(buffer);
    //Serial.print("Analog: ");
    //Serial.println(analogRead(A1));
    Serial.print("V");
    Serial.print(vol_accurate);
    Serial.println("#");
    
    prevV = millis();
  }
}
 
void handleSerial() {
  if(stringComplete) {
    Serial.println(inputString.c_str());
    stringComplete = false;
    inputString = "";
  }
}

void serialEvent()
 {
  while(Serial.available())
  {
    char dir_input = Serial.read();
    

//Raising the Mast. (U for up)
    if(dir_input == 'U' || dir_input == 'u')
    {
       while(analogRead(pot_pin)<870){
            analogWrite(pwm, 255);
            digitalWrite(dir, LOW);
            Serial.print("UP |");
            Serial.print(analogRead(pot_pin));
            Serial.println();
      }
      analogWrite(pwm, 0); 
    }

//Lowering the Mast. (D for down)
    else if(dir_input == 'D' || dir_input == 'd')
    {
        while(analogRead(pot_pin)>30){
              analogWrite(pwm, 255);
              digitalWrite(dir, HIGH);
              Serial.print("DOWN |");
              Serial.print(analogRead(pot_pin));
              Serial.println();
        }
        analogWrite(pwm, 0);
    }
  }
 }