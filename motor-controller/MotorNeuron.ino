#include <Servo.h> 
#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 255

typedef struct {
	char str[MAX_STRING_LENGTH];
	byte len;
} Buffer;

Servo motor;  // create servo object to control a servo 
boolean rx_complete = false;
Buffer rx;
char output[100] = {0};
int pos = 90;

void clearBuffer(Buffer * buf) {
	memset(buf->str, 0, buf->len);
	buf->len = 0;
}

boolean BufferAppend(Buffer * buf, char c) {
	if(buf->len < MAX_STRING_LENGTH) {
		buf->str[buf->len] = c;
		++buf->len;
		return true;	
	}
	return false;
}

void setup() {
	Serial.begin(115200);
	Serial.println("Ready!");
	motor.attach(A0);
	motor.write(pos);
}

void loop() {
	if(rx_complete) {
		sprintf(output, "[rx_c] %s\n", rx.str);
		Serial.println(output);
		sscanf(rx.str, "%d", &pos);
		if(pos < 0 || pos > 180) {
			Serial.println("Server/Motor Position must be between 1 and 180.");
		} else {
			motor.write(pos);	
		}
		clearBuffer(&rx);
		Serial.flush();
		rx_complete = false;
	}
}

void serialEvent() {
	boolean appended = false;
	while (Serial.available()) {
		char c = (char)Serial.read();
		appended = BufferAppend(&rx, c);
		if (!appended || c == '\n' || c == '~') {
			rx_complete = true;
		} 
	}
}