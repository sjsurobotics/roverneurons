#!/bin/bash

if [ $# -eq 0 ]
  then
	echo "Please input a .hex file to load on to the arduino."
	exit
fi

if [ -z "$1" ]
  then
	echo "No argument supplied"
	exit
fi

echo "Setting up GPIO 11, P8_32 UART5_RTS pin"
#arduino reset pin, pull high to turn on
#enable the pin
echo 11 > /sys/class/gpio/export
#set for output
echo "out" > /sys/class/gpio/gpio11/direction
#set HIGH
echo 1 > /sys/class/gpio/gpio11/value

#Set to Bootloader mode (fork)
(echo 0 > /sys/class/gpio/gpio11/value && sleep 0.9 && echo 1 > /sys/class/gpio/gpio11/value) &

avrdude -p atmega328p -c arduino -P /dev/ttyO5 -b 57600 -D -U flash:w:$1:i

