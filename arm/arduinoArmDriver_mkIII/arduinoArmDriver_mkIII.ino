// arduinoARM.ino
#include <SoftwareSerial.h>
#include <ctype.h> //tolower
#define maxStatusLength 11 //Length of Status Packet

//Ports
#define SERV_RX 2 //RO (receiving data)
#define SERV_TX 3 //DI (transmit data)
#define READ_ENABLE 4 //RE
#define WRITE_ENABLE 5 //DE
//Instructions
#define WRITE 0x03//3 //Write command
#define READ 0x02//2 //Read command
//Motor Ids
#define toAll 0xFE //254 //Braodcast ID
//Register Ids
#define POS 0x1E//30 //Goal Position (Start of Low Byte)
#define TORQUE 0x18//24 //Torque Enable
#define LED 0x19//25 //LED On/Off
#define IDENTITY 0x03//3 //Register Address for ID of motor
#define CURRENT_POS 0x24//36 //Present Position of Motor
#define CW 0x06 //6 //CW Angle Limit lowbyte
#define CCW 0x08 //8 //CCW Angle Limit lowbyte
//Constants
#define ON 0x01
#define OFF 0x00

/*Globlas*/
boolean lit = false; //light is on/off
boolean showstatus = true;
boolean moveable = false; //torque on/off
int operationMode = 0; //0 = wheel mode, 1 = joint mode
//StatusPacket Globals
boolean keeptrack = false;
short elements = 0;
byte Status[maxStatusLength]; //status packet, of max length maxStatusLength
short statuslength = 0; //Holds the length parameter of the status packet
short statusParamNum = 0; //Number of status parameters read/received
char response = ' '; //Servo motor feedback/response
//InstructionPacket Globals
int idnum = 1; //Id number for motor
int motortype = 0; //0 = MX, 1 = RX
String ID = "";
String degree = "";
char input = ' '; //User Input
//int mode = 0; //0 = command, 1 = motor id, 2 = value(0-360 degree, or 1/0 for on/off)

SoftwareSerial servo(SERV_RX, SERV_TX); // RX, TX
// Functions
void writePacket(int instruction, int motorID, int registration, int lower, int higher = -1); //Universal write function//for moving servo/ anything with 3 params (addr, lowbyte, highbyte)

void setup() {
	servo.begin(57600);
	Serial.begin(9600); //For Debugging
        Serial.print("===DYNAMIXEL mx-64AR/rx-64 Motor Driver===\n");
        Serial.println("type degree in format: <degree Num>*");
        pinMode(4, OUTPUT); //required to control READenable/WRITEenable pins in rs485
        pinMode(5, OUTPUT);
        delay(100);
        digitalWrite(READ_ENABLE, HIGH);
        digitalWrite(WRITE_ENABLE, HIGH);
        
        Serial.println("Default: Torque OFF");
        writePacket(WRITE, idnum, TORQUE, OFF);
        moveable = false;
}

void loop() {
//Manual Motor Control - Degree Input
//        Serial.println("start");
//        writePacket(WRITE, idnum, POS, 0x00, 0x04); //90*
//        delay(2000);
//        writePacket(WRITE, idnum, POS, 0xFF, 0x0F); //360*
//        delay(2000);
//                Serial.println("End");
        if(showstatus){ //Current Program Status Indicator
          Serial.print("\n===DYNAMIXEL mx-64AR/rx-64 Motor Driver===\n");
          Serial.print("Light:");
          (lit) ? Serial.print("On") : Serial.print("Off"); //Inline if-statement for light
          Serial.print("\tTorque:");
          (moveable) ? Serial.print("On\n") : Serial.print("Off\n"); //Inline if-statement for torque
          Serial.print("OpMode:");
          if (operationMode == 0) {Serial.print("Wheel-Mode");} else if (operationMode == 1) {Serial.print("Joint-Mode");} else {Serial.print("N/A");}
          Serial.print("\tControlling:");
          Serial.println(idnum);
          Serial.print("Type:");
          if (motortype == 0) {Serial.println("MX-64AR");} else if (motortype == 1) {Serial.println("RX-64");}
          showstatus = false;
        }

        if(Serial.available()){
          input = (char) Serial.read(); //for char input
          if(input == '*'){                     //Move to a spefied Degree Input
            float pos = (float)degree.toInt();
            float goal = 0;
            if(motortype == 0){ //MX-64AR
              goal = (pos/360) * 4095; //for MX-64AR
              if(goal > 4095){
                goal = 4095;
              }
            }
            else if(motortype == 1){ //RX-64
              goal = (pos/300) * 1023;
              if(goal > 1023){
                goal = 1023;
              }
            }
            
            Serial.print(">>Moving to Position: ");
            Serial.println(pos);
            Serial.print("    Goal: ");
            Serial.print(goal);
          //Form low and high bytes
            short val = (short)goal;
            byte hi = byte(val >> 8);
            byte lo = byte(val & 0x00FF) ; 
            Serial.print("  lo: ");
            Serial.print(lo);
            Serial.print("  hi: ");
            Serial.println(hi);
            writePacket(WRITE, idnum, POS, lo, hi); //Move motor of ID idnum to pos
//            Clear Variables for reuse
            pos = 0;
            degree = "";
            showstatus = true;
          }
          else if(input == 't' || input == 'T'){ //torque toggle
            if(moveable == false){
              Serial.println(">>Turning torque on");
              writePacket(WRITE, idnum, TORQUE, ON);
              moveable = true;
            }
            else if(moveable == true){
              Serial.println(">>Turning torque off");
              writePacket(WRITE, idnum, TORQUE, OFF);
              moveable = false;
            }
            degree = "";
            showstatus = true;
          }
          else if(input == 'm' || input == 'M'){ //Check if moveable
            Serial.println(">>Move status:");
            writePacket(READ, idnum, TORQUE, 1);
            degree = "";
            showstatus = true;
          }
          else if(input == 'l' || input == 'L'){ //Light Toggle
            if(lit == false){
              Serial.println(">>Turning on light");
              writePacket(WRITE, idnum, LED, ON);
              lit = true;
            }
            else if(lit == true){
              Serial.println(">>Turning off light");
              writePacket(WRITE, idnum, LED, OFF);
              lit = false;
            }
            degree = "";
            showstatus = true;
          }
          else if(input == 'r' || input == 'R'){ //Factory Reset to Default Settings
            Serial.println(">>Factory Reset");
            servo.write((byte)0xFF); //sync byte
            servo.write((byte)0xFF); //start byte
            servo.write((byte)0xFE); //ID byte
            servo.write((byte)0x02); //length byte
            servo.write((byte)0x06); //instruction byte
            servo.write((byte)0xF9); //Checksum
            degree = "";
            showstatus = true;
          }
          else if(tolower(input) == 'o'){ //Change Operation Mode
//            if(operationMode == 0){ //i.e. is in wheel mode
//              Serial.println("Changing to Joint Mode");
//              writePacket(WRITE, idnum, CW, 0x00,0x00);
//              writePacket(WRITE, idnum, CCW, 0xFF,0x0F);
//              operationMode = 1;
//            }
//            else if(operationMode == 1){ //i.e. is in joint mode
//              Serial.println("Changing to Wheel Mode");
//              writePacket(WRITE, idnum, CW, 0x00,0x00);
//              writePacket(WRITE, idnum, CCW, 0x00,0x00);
//              operationMode = 0;
//            }
//              Serial.println("Setting to RX-64 Limits");
//              writePacket(WRITE, idnum, CW, 0x56,0x01); //lower limit 30*
//              writePacket(WRITE, idnum, CCW, 0xA9,0x0E); //upper limit 330*
              Serial.println(">>Setting to MultiTurn Mode");
              writePacket(WRITE, idnum, CW, 0xFF,0x0F); 
              writePacket(WRITE, idnum, CCW, 0xFF,0x0F);
              degree = "";
              showstatus = true;              
          }
          else if(input == 'i' || input =='I'){ //Identify Motor
            Serial.println(">>Motor ID:");
            writePacket(READ,idnum,IDENTITY,1); //Read, motor 1, identity register address, only 1 to read
//            servo.write(byte
            degree = "";
            showstatus = true;
          }
          else if(input == 'c' || input == 'C'){ //Change Motor ID number
            float newIDNum = (float)degree.toInt();
            if(newIDNum < 0 || newIDNum > 254){
              Serial.print(">>ID out of range 0 - 254");
            }
            else{
              Serial.print(">>Changing motor ID from ");
              Serial.print(idnum);
              Serial.print(" to ");
              Serial.println(newIDNum);
              writePacket(WRITE, idnum, IDENTITY, newIDNum);
            }
            degree = "";
            showstatus = true;
          }
          else if(input == 'p' || input == 'P'){ //Show current position
            Serial.println(">>Current Position:");
            writePacket(READ,idnum,CURRENT_POS,2);
            degree = "";
            showstatus = true;
          }
          //==============================================================
          else if(input == 'x' || input == 'X'){ //Motor Type: Change between RX and MX motors
            int newmode = degree.toInt();
            if(newmode == 1){
              motortype = 1; //RX-64
              Serial.println(">>Operation Mode: RX-64");
            }
            else if(newmode == 0){
              motortype = 0; //MX-64AR
              Serial.println(">>Operation Mode: MX-64AR");
            }
            else{
              Serial.println(">>Invalid Motor Type");
            }
            degree = "";
            showstatus = true;
          }
          else if(input == '='){ //change id number for arduino to control
            int whichMotor = degree.toInt();
            if(whichMotor < 0 || whichMotor > 254){
              Serial.println(">>Invalid: Motor ID number out of range");             
            }
            else{
              idnum = degree.toInt();
              Serial.print(">>Now controlling Motor ");
              Serial.println(idnum);              
            }
            degree = "";
            showstatus = true;
          }
          else if(isdigit(input)){
            degree += input; //Append to input string
          }
          else{
            Serial.println(">>Invalid");
          }
        }
}


void test() {
Serial.println("Turning on light");
        writePacket(WRITE,idnum, LED, ON);
        delay(1000);
        Serial.println("Turning on Torque");
        writePacket(WRITE, idnum, TORQUE, ON);
        delay(1000);
        Serial.println("Moving to Position 270");
        writePacket(WRITE, idnum, POS, 0xFF, 0x0B);
        delay(1000);
        Serial.println("Moving to Position 360");
        writePacket(WRITE, idnum, POS, 0xFF, 0x0F);
        delay(1000);
        Serial.println("Moving to Position 0");
        writePacket(WRITE, idnum, POS, 0x00, 0x00);
        delay(1000);
        Serial.println("Turning off light");
        writePacket(WRITE, idnum, LED, OFF);
        delay(1000);
}


//Function Definitions
void writePacket(int instruction, int motorID, int registration, int lower, int higher){ //Meant for any packet with 3 params (addr, low, high)//Default of higher == -1
  byte ID = byte(motorID); //motor id
  byte command = byte(instruction); //instruction byte
  byte address = byte(registration); //address of goal position (L)
  byte lowbyte = byte(lower); //lowbyte
  byte highbyte = byte(higher); //highbyte
  byte packetlength = 0;
  byte len = 0; //length of packet (should be 5)
  byte checksum = 0; //checksum
//    //For Debugging
//    Serial.print("  higher: ");
//    Serial.print(higher,DEC);
//    Serial.println("");
  if(higher >= 0) {
    packetlength = 3 + 2; //num of parameters, including instruction byte and checksum byte  byte ID = byte(motorID);
    len = byte(packetlength); //length of packet (should be 5)
    checksum = ~(ID + len + command + address + lowbyte + highbyte); //checksum
  } else {
    packetlength = 2 + 2; //num of parameters, including instruction byte and checksum byte  byte ID = byte(motorID);
    len = byte(packetlength); //length of packet (should be 5)
    checksum = ~(ID + len + command + address + lowbyte); //checksum
  }
  /*ReadWrite Pins must be on to write data*/
  digitalWrite(READ_ENABLE, HIGH);
  digitalWrite(WRITE_ENABLE, HIGH); 
  
  if(higher == -1){
    servo.write((byte)0xFF); //sync byte
    servo.write((byte)0xFF); //start byte
    servo.write((byte)ID); //ID byte
    servo.write((byte)len); //length byte
    servo.write((byte)command); //instruction byte
    servo.write((byte)address); //Register address of goal position (L)
    servo.write((byte)lowbyte); //Lowbyte val
    servo.write((byte)checksum); //Checksum
//    //For Debugging
//    Serial.print("  instruction packet: ");
//    Serial.print(0xFF,DEC); //sync byte
//    Serial.print(" ");
//    Serial.print(0xFF,DEC); //start byte
//    Serial.print(" ");
//    Serial.print(ID,DEC); //ID byte
//    Serial.print(" ");
//    Serial.print(len,DEC); //length byte
//    Serial.print(" ");
//    Serial.print(command,DEC); //instruction byte
//    Serial.print(" ");
//    Serial.print(address,DEC); //Register address of goal position (L)
//    Serial.print(" ");
//    Serial.print(lowbyte,DEC); //Lowbyte val
//    Serial.print(" ");
//    Serial.print(checksum,DEC); //Checksum
//    Serial.println("");
  }
  else{
    servo.write((byte)0xFF); //sync byte
    servo.write((byte)0xFF); //start byte
    servo.write((byte)ID); //ID byte
    servo.write((byte)len); //length byte
    servo.write((byte)command); //instruction byte
    servo.write((byte)address); //Register address of goal position (L)
    servo.write((byte)lowbyte); //Lowbyte val
    servo.write((byte)highbyte); //Highbyte val  
    servo.write((byte)checksum); //Checksum
//    //For Debugging    
//    Serial.print("  instruction packet: ");
//    Serial.print(0xFF,DEC); //sync byte
//    Serial.print(" ");
//    Serial.print(0xFF,DEC); //start byte
//    Serial.print(" ");
//    Serial.print(ID,DEC); //ID byte
//    Serial.print(" ");
//    Serial.print(len,DEC); //length byte
//    Serial.print(" ");
//    Serial.print(command,DEC); //instruction byte
//    Serial.print(" ");
//    Serial.print(address,DEC); //Register address of goal position (L)
//    Serial.print(" ");
//    Serial.print(lowbyte,DEC); //Lowbyte val
//    Serial.print(" ");
//    Serial.print(highbyte,DEC); //Highbyte val
//    Serial.print(" ");
//    Serial.print(checksum,DEC); //Checksum
//    Serial.println("");
  }
  /*ReadWrite Pins must be off to write data*/
  digitalWrite(READ_ENABLE, LOW);
  digitalWrite(WRITE_ENABLE, LOW);
  delay(50);
  Serial.print("    Response: ");
  byte temps[20];
  int iterator = 0;
  while(servo.available()) {
    temps[iterator] = (byte) servo.read();
    iterator++;
  }
  for(int g = 0; g < iterator; g++){
    Serial.print(temps[g], BIN);
    Serial.print(",");
  }
  Serial.print("\n    Hex: ");
  for(int g = 0; g < iterator; g++){
    Serial.print(temps[g], HEX);
    Serial.print(",");
  }
  Serial.print("\n    Dec: ");
  for(int g = 0; g < iterator; g++){
    Serial.print(temps[g], DEC);
    Serial.print(",");
  }
  iterator = 0;
  
  Serial.println(" ");
//  delay(500);
}
