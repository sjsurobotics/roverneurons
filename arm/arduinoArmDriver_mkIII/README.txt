Arduino Arm Motor Driver (MX-64AR/RX-64) Read-Me
================================================
Materials Needed: Dynamixel Cord, Arduino, Arduino USB cable, Jumper wires (an assortment of enough male-female, male-male, female-female), TTL-RS485 Converter Module, external power supply (Motors need 12v)
Connections:
a.) Connect TTL-RS485 Converter Module
	-Module RO to Digital2
	-Module DI to Digital3
	-Module RE to Digital4
	-Module DE to Digital5
	-Module VCC to Arduino 5v
	-Module GND to Arduino GND
b.) Connect Motor - See Online Dyamixel Communications manual (http://support.robotis.com/en/techsupport_eng.htm#product/dynamixel/mx_series/mx-64.htm) for pin assignments
	-Motor VDD to power supply 12v
	-Motor GND to power supply GND
	-Motor D+ to Module pin A
	-Motor D- to Module pin B	

This program comes with functions that allow the user to manipulate and communicate with a servo in several different ways, as described here:

1.) Angular Position Control
	"Allows you to feed in a goal position value (within the motor's specified bounds) to move the motor."
	Format: [degree]*
	Ex: Typing 150* and hitting enter on the Arduino Serial Monitor will have the arduino send a command to the servo telling it to go to 150 degrees.
	Note: Each type of motor has different bounds, RX: 0-300 (150 vertical), MX: 0-360 (180 vertical)

2.) Torque Toggle
	"Allows you to toggle the servo's torque"
	Format: t
	Ex: Simply type the letter t (upper or lowercase), and hit enter, and torque will toggle on/off depending on your previous torque status
	Note: This program defaults to have torque set to Off, although I've noticed that this function proves to be ineffective at preventing movement at times.

3.) LED Toggle
	"Allows you to toggle the LED on/off"
	Format: l
	Ex: Simply type the letter l (upper or lowercase), and hit enter, and the light will toggle on/off depending on your previous light status

4.) Factory Reset
	"Allows you to send a factory reset command to the servos to have then start at default settings"
	Format: r
	Ex: Simply type the letter r (upper or lowercase), and hit enter, and the command will be sent.
	Note: I've experienced mixed results with this command, as sometimes the motors don't abide by this command when sent. Also, default settings entail that
	the ID of the motor being manipulated must be changed to 1.

5.) Operation Mode Change
	"Allows you to change between servo operation modes, namely wheel mode, joint mode, and multi turn mode"
	Format: o
	Ex: Simply type letter o (case-insensitive) and enter to switch.
	Note: Changing this feature thereby changes the way the motor moves and governs its positions. I suggest refraining from using this function at all unless
	you know what you're doing.

6.) Identify Motor
	"Allows you to ask the motor of it's ID number"
	Format: i
	Ex: Simply type letter i (case-insensitive) and enter to ask.
	Note: You will get back a packet of numbers in return. Since I haven't yet made any code that can translate these packets to a readable return message, read
	the Dynamixel's communication 1.0 manual online about status packets

7.) Change Motor ID Number
	"Allows you to change the ID number of a motor's flash memory"
	Format: [number]c
	Ex: Typing 23c and hitting enter will tell the motor to change ID from its previous value to 23, thereby making this motor #23
	Note: Both types of motors can only have ID numbers between 0 and 254.

8.) Reading Current Position
	"Allows the user to get the current position from the motor"
	Format: p
	Ex: Simply type letter p (case-insensitive) and enter to receive the status packet containing the highbyte and lowbyte of the position
	Note: The position values are split into two different sections, since packet elements can only be one byte long. Referring to the manual provided by Dynamixel
	online, the position value (in hexadecimal) should be the combination of the last two PARAMETER elements of the status packet. Of the two elements, the highbyte
	is the one closest to the last element in the packet, and that byte represents the digits of the hexadecimal number that are that are >= 255, and the remaining
	element in the packet is the lowbyte, representing the values from 0 - 254. Putting them together, you have the 3 digit hexadecimal number representing your angular
	position. I.e. For MX-64AR, you may get a packet containing [0xFF,0xFF,....,0x00,0x08, 0x9D] where your lowbyte is 0x00 and highbyte is 0x08. Combine the highbyte
	and lowbyte to get 0x0800, which represents the decimal number 2048. The positional values for the MX-64AR span numbers 0 to 4095, representing 0 and 360,
	respectively. The hexnumber value "Goal" to feed to servos for goal position is calculated by the following formula:
		
		Goal = (deg/max)*span

	where 'deg' is the desired position in degrees (0-300 or 0-360 depending on the motor used), max is the maximum degree value that the motor you are controlling can
	reach (300 for RX, 360 for MX), and span is the maximum span of hexnumber values that the motor can go through (4095 for MX, 1023 for RX). Therefore, since the number
	we received happened too be 2048, we then have 2048 = (deg/360)*4095, and therefore we find that the motor's current position is (2048/4095)*360 = 180 degrees.

9.) Change Motor Type
	"Allows user to change control protocols between RX and MX, which will then enable positional control across different motor types"
	Format: [number]x (case-insensitive)
	Ex: Typing 0x and enter will change motor control protocols to fit control for MX-64AR. Typing 1x will change to fit RX control.

10.) Change Which Motor You Are Controlling 
	"Allows user to change which motor is being contolled"
	Format: [number]=
	Ex: Typing in 32= and enter will switch the program from controlling previous motor to the motor with ID #32.
	Note: The program defaults to ID#1
